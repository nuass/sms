USE [master]
GO
/****** Object:  Database [SMSDB]    Script Date: 06/10/2015 23:07:48 ******/
CREATE DATABASE [SMSDB] ON  PRIMARY 
( NAME = N'SMSDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\SMSDB.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SMSDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\SMSDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SMSDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SMSDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SMSDB] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [SMSDB] SET ANSI_NULLS OFF
GO
ALTER DATABASE [SMSDB] SET ANSI_PADDING OFF
GO
ALTER DATABASE [SMSDB] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [SMSDB] SET ARITHABORT OFF
GO
ALTER DATABASE [SMSDB] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [SMSDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [SMSDB] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [SMSDB] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [SMSDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [SMSDB] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [SMSDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [SMSDB] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [SMSDB] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [SMSDB] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [SMSDB] SET  DISABLE_BROKER
GO
ALTER DATABASE [SMSDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [SMSDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [SMSDB] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [SMSDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [SMSDB] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [SMSDB] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [SMSDB] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [SMSDB] SET  READ_WRITE
GO
ALTER DATABASE [SMSDB] SET RECOVERY SIMPLE
GO
ALTER DATABASE [SMSDB] SET  MULTI_USER
GO
ALTER DATABASE [SMSDB] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [SMSDB] SET DB_CHAINING OFF
GO
USE [SMSDB]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 06/10/2015 23:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[Picture] [nvarchar](50) NULL,
	[Deleted] [date] NULL,
	[Created] [date] NULL,
	[Modified] [date] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teachers]    Script Date: 06/10/2015 23:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teachers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StaffID] [int] NULL,
	[FullName] [nvarchar](50) NULL,
	[Sex] [nvarchar](50) NULL,
	[Age] [tinyint] NULL,
	[Description] [nvarchar](50) NULL,
	[Picture] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Deleted] [date] NULL,
	[Created] [date] NULL,
	[Modified] [date] NULL,
 CONSTRAINT [PK_Teachers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Students]    Script Date: 06/10/2015 23:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RegisstrationNumber] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[Sex] [nvarchar](50) NULL,
	[Age] [tinyint] NULL,
	[Address] [nvarchar](50) NULL,
	[ContactPhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Class] [nvarchar](50) NULL,
	[Stream] [nvarchar](50) NULL,
	[Picture] [nvarchar](50) NULL,
	[Deleted] [date] NULL,
	[Created] [date] NULL,
	[Modified] [date] NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fee]    Script Date: 06/10/2015 23:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationNumber] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[Class] [nvarchar](50) NULL,
	[Free] [numeric](18, 2) NULL,
	[FreePaid] [numeric](18, 2) NULL,
	[Balance] [numeric](18, 2) NULL,
	[Status] [nvarchar](50) NULL,
	[Created] [date] NULL,
	[Modified] [date] NULL,
 CONSTRAINT [PK_Fee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Assessment]    Script Date: 06/10/2015 23:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assessment](
	[ID] [int] NULL,
	[RegistrationNumber] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[Class] [nvarchar](50) NULL,
	[Stream] [nvarchar](50) NULL,
	[Subject] [nvarchar](50) NULL,
	[CA] [numeric](18, 0) NULL,
	[Exams] [numeric](18, 0) NULL,
	[Total] [numeric](18, 0) NULL,
	[Grade] [nvarchar](50) NULL,
	[Position] [nvarchar](50) NULL,
	[Remarks] [nvarchar](50) NULL,
	[Deleted] [date] NULL,
	[Created] [date] NULL,
	[Modiferd] [date] NULL
) ON [PRIMARY]
GO
