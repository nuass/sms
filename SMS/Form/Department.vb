﻿Imports System.Data.SqlClient

Public Class Department

    Private Sub Department_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Main.Show()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Department_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DepartmentDataset.Departments' table. You can move, or remove it, as needed.
        Me.DepartmentsTableAdapter.Fill(Me.DepartmentDataset.Departments)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If txtName.Text = "" Then
            txtName.Focus()
            Label5.Text = "This field Requred"
        Else
            Label5.Text = ""

            Try
                SMSFucition.cnn()
                query = "INSERT INTO Departments (Name,Manager,Location,Phone) VALUES ('" & txtName.Text & "','" & txtManager.Text & "','" & txtLocation.Text & "','" & txtPhone.Text & "')"
                cmd = New SqlCommand(query, cn)
                cmd.ExecuteNonQuery()
                Me.DepartmentsTableAdapter.Fill(Me.DepartmentDataset.Departments)
                lblMessage.Text = "Insert Success"
            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                cn.Close()
                txtLocation.Text = ""
                txtManager.Text = ""
                txtPhone.Text = ""
                txtName.Text = ""
            End Try
        End If
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If lblMessage.Visible = True Then
            lblMessage.Visible = False
        End If

    End Sub

    Private Sub txtName_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged
        Label5.Text = ""
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            SMSFucition.cnn()
            query = "DELETE FROM Departments WHERE ID=" & DataGridView1.CurrentRow.Cells(0).Value & ""
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            If Me.DataGridView1.SelectedRows.Count > 0 Then
                DataGridView1.Rows.RemoveAt(Me.DataGridView1.SelectedRows(0).Index)
            End If
        Catch ex As Exception
            MessageBox.Show("Can't delete this department becuase this department contain student(s).")
        Finally
            txtLocation.Text = ""
            txtManager.Text = ""
            txtPhone.Text = ""
            txtName.Text = ""
            cn.Close()
        End Try

    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        txtName.Text = DataGridView1.CurrentRow.Cells(1).Value.ToString()
        txtManager.Text = DataGridView1.CurrentRow.Cells(2).Value.ToString()
        txtLocation.Text = DataGridView1.CurrentRow.Cells(3).Value.ToString()
        txtPhone.Text = DataGridView1.CurrentRow.Cells(4).Value.ToString()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            SMSFucition.cnn()
            query = "UPDATE Departments SET Name='" & txtName.Text & "',Manager='" & txtManager.Text & "',Location='" & txtLocation.Text & "',Phone='" & txtPhone.Text & "'  WHERE ID=" & DataGridView1.CurrentRow.Cells(0).Value & ""
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            Me.DepartmentsTableAdapter.Fill(Me.DepartmentDataset.Departments)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub lblMessage_Click(sender As Object, e As EventArgs) Handles lblMessage.Click

    End Sub

    Private Sub Label12_Click(sender As Object, e As EventArgs) Handles Label12.Click

    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs) Handles Label5.Click

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub Label9_Click(sender As Object, e As EventArgs) Handles Label9.Click

    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            SMSFucition.cnn()
            query = "SELECT * FROM Departments WHERE ID=" & txtSearch.Text & ""
            Dim Ad As New SqlDataAdapter(query, cn)
            Dim dataset As New DataSet()
            Ad.Fill(dataset, "Emp")
            DataGridView1.DataSource = dataset.Tables(0)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Search Error")
        Finally
            cn.Close()
        End Try

    End Sub

    Private Sub txtManager_TextChanged(sender As Object, e As EventArgs) Handles txtManager.TextChanged

    End Sub

    Private Sub txtLocation_TextChanged(sender As Object, e As EventArgs) Handles txtLocation.TextChanged

    End Sub

    Private Sub txtPhone_TextChanged(sender As Object, e As EventArgs) Handles txtPhone.TextChanged

    End Sub

    Private Sub TextBox4_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged

    End Sub
End Class