﻿Imports System.Data.SqlClient

Public Class Student

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sex As String = ""
        If rdbFemale.Checked Then
            sex = "Female"
        Else
            sex = "Male"
        End If

        Dim cnn As New SqlConnection("server=.\SQLEXPRESS; Database=SchoolDb; Integrated Security=true")
        Try
            cnn.Open()
            '  Dim query = "INSERT INTO Students (Name,DepartmentID) VALUES ('" & txtName.Text & "'," & cmbDepartment.SelectedValue & ")"
            Dim query = "INSERT INTO Students (Name,Sex,DepartmentID,Batch,Shift,[Year],[Address],Phone,Photo,Notes,DateOfBirth) VALUES ('" & txtName.Text & "','" & sex & "', " & cmbDepartment.SelectedValue & "," & txtBatch.Text & ",'" & CmbShift.SelectedText & "'," & txtYear.Text & " ,'" & txtAddress.Text & "','" & txtPhone.Text & "','" & OpenFileDialog1.FileName & "','" & txtNote.Text & "','" & DateOfBirth.Text & "')"
            Dim cmd As New SqlCommand(query, cnn)
            MessageBox.Show(query)
            cmd.ExecuteNonQuery()
            MessageBox.Show("Insert success")
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cnn.Close()
            Me.Close()
        End Try
    End Sub

    Private Sub Student_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SchoolDbDataSet.Departments' table. You can move, or remove it, as needed.
        Me.DepartmentsTableAdapter.Fill(Me.SchoolDbDataSet.Departments)
        Dim list As New ArrayList
        list.Add("Morning")
        list.Add("Afternoon")
        list.Add("Evengin")
        CmbShift.AutoCompleteSource = AutoCompleteSource.ListItems
        CmbShift.AutoCompleteMode = AutoCompleteMode.Append
        For Each item As String In list
            CmbShift.Items.Add(item)
        Next
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged, txtNote.TextChanged, txtPhone.TextChanged, txtYear.TextChanged, txtAddress.TextChanged, txtBatch.TextChanged

    End Sub
    Dim depId As Integer
    Private Sub cmbDepartment_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim cnn As New SqlConnection("server=.\SQLEXPRESS; Database=SchoolDb; Integrated Security=true")

        Try
            cnn.Open()
            Dim query = "SELECT ID FROM Departments WHERE Name = '" & cmbDepartment.SelectedItem & "'  "
            Dim cmd As New SqlCommand(query, cnn)
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read()
                depId = reader(0)
                MessageBox.Show(depId)
            End While
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cnn.Close()
        End Try
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click, Label12.Click

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub TextBox1_TextChanged_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDepartment.SelectedIndexChanged

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        With OpenFileDialog1
            .Title = "Select student image"
            .Multiselect = False
            .Filter = "Insert Image file as |*.jpg|PNG|*.png|GIF|*.gif "

            .FileName = ""
            .ShowDialog()
            If .FileName = "" Then
                Exit Sub
            Else
                PictureBox1.Load(.FileName)
            End If
        End With

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        PictureBox1.Image = Nothing

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Student_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        '    StudentListForm.Close()
        StudentListForm.Show()
    End Sub
End Class