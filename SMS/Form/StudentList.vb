﻿Imports System.Data.SqlClient

Public Class StudentListForm
    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SchoolDbDataSet1.Departments' table. You can move, or remove it, as needed.
        Me.DepartmentsTableAdapter1.Fill(Me.SchoolDbDataSet1.Departments)
        'TODO: This line of code loads data into the 'StudentListDataSet1.StudentDepartmentView' table. You can move, or remove it, as needed.
        Me.StudentDepartmentViewTableAdapter.Fill(Me.StudentListDataSet1.StudentDepartmentView)
        Dim list As New ArrayList
        list.Add("Morning")
        list.Add("Afternoon")
        list.Add("Evengin")
        CmbShift.AutoCompleteSource = AutoCompleteSource.ListItems
        CmbShift.AutoCompleteMode = AutoCompleteMode.Append
        For Each item As String In list
            CmbShift.Items.Add(item)
        Next
    End Sub

    Private Sub DataGridView1_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sex As String = ""
        If rdbFemale.Checked Then
            sex = "Female"
        Else
            sex = "Male"
        End If
        Try
            SMSFucition.cnn()
            query = "INSERT INTO Students (Name,Sex,DepartmentID,Batch,Shift,[Year],[Address],Phone,Photo,Notes,DateOfBirth) VALUES ('" & txtName.Text & "','" & sex & "', " & cmbDepartment.SelectedValue & "," & txtBatch.Text & ",'" & CmbShift.SelectedText & "'," & txtYear.Text & " ,'" & txtAddress.Text & "','" & txtPhone.Text & "','" & OpenFileDialog1.FileName & "','" & txtNote.Text & "','" & DateOfBirth.Text & "')"
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            Me.StudentDepartmentViewTableAdapter.Fill(Me.StudentListDataSet1.StudentDepartmentView)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cn.Close()
            txtName.Text = ""
            txtAddress.Text = ""
            txtBatch.Text = ""
            txtNote.Text = ""
            txtPhone.Text = ""
            txtYear.Text = ""
            PictureBox1.Image = Nothing
            DateOfBirth.Text = ""
        End Try
    End Sub

    Private Sub Student_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SchoolDbDataSet.Departments' table. You can move, or remove it, as needed.
        'Me.DepartmentsTableAdapter.Fill(Me.SchoolDbDataSet.Departments)
        'Dim list As New ArrayList
        'list.Add("Morning")
        'list.Add("Afternoon")
        'list.Add("Evengin")
        'CmbShift.AutoCompleteSource = AutoCompleteSource.ListItems
        'CmbShift.AutoCompleteMode = AutoCompleteMode.Append
        'For Each item As String In list
        '    CmbShift.Items.Add(item)
        'Next
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged, txtNote.TextChanged, txtPhone.TextChanged, txtYear.TextChanged, txtAddress.TextChanged, txtBatch.TextChanged

    End Sub
    Dim depId As Integer
    Private Sub cmbDepartment_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim cnn As New SqlConnection("server=.\SQLEXPRESS; Database=SchoolDb; Integrated Security=true")

        Try
            cnn.Open()
            Dim query = "SELECT ID FROM Departments WHERE Name = '" & cmbDepartment.SelectedItem & "'  "
            Dim cmd As New SqlCommand(query, cnn)
            Dim reader As SqlDataReader = cmd.ExecuteReader
            While reader.Read()
                depId = reader(0)
                MessageBox.Show(depId)
            End While
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cnn.Close()
        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Try
            SMSFucition.cnn()
            query = "SELECT * FROM Students WHERE ID=" & txtSearch.Text & ""
            Dim Ad As New SqlDataAdapter(query, cn)
            Dim dataset As New DataSet()
            Ad.Fill(dataset, "Emp")
            DataGridViewStudent.DataSource = dataset.Tables(0)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Search Error")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub StudentListForm_FormClosing(sender As Object, e As FormClosingEventArgs)

    End Sub

    Private Sub FillByToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            Me.DepartmentsTableAdapter.FillBy(Me.SchoolDbDataSet.Departments)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        With OpenFileDialog1
            .Title = "Select student image"
            .Multiselect = False
            .Filter = "Insert Image file as |*.jpg|PNG|*.png|GIF|*.gif "

            .FileName = ""
            .ShowDialog()
            If .FileName = "" Then
                Exit Sub
            Else
                PictureBox1.Load(.FileName)
            End If
        End With
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        PictureBox1.Image = Nothing
    End Sub

    Private Sub StudentListForm_FormClosed(sender As Object, e As FormClosedEventArgs)
        Me.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            SMSFucition.cnn()
            query = "DELETE FROM Students WHERE ID=" & DataGridViewStudent.CurrentRow.Cells(0).Value & ""
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            If Me.DataGridViewStudent.SelectedRows.Count > 0 Then
                DataGridViewStudent.Rows.RemoveAt(Me.DataGridViewStudent.SelectedRows(0).Index)
            End If
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Can't delete this department becuase this department contain student(s).")
        Finally
            txtName.Text = ""
            txtAddress.Text = ""
            txtBatch.Text = ""
            txtNote.Text = ""
            txtPhone.Text = ""
            txtYear.Text = ""
            PictureBox1.Image = Nothing
            DateOfBirth.Text = ""
            cn.Close()
        End Try
    End Sub

    Private Sub DataGridViewStudent_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewStudent.CellContentClick

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        With OpenFileDialog1
            .Title = "Select student image"
            .Multiselect = False
            .Filter = "Insert Image file as |*.jpg|PNG|*.png|GIF|*.gif "

            .FileName = ""
            .ShowDialog()
            If .FileName = "" Then
                Exit Sub
            Else
                PictureBox1.Load(.FileName)
            End If
        End With
    End Sub

    Private Sub DataGridViewStudent_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewStudent.CellClick
        txtName.Text = DataGridViewStudent.CurrentRow.Cells(1).Value.ToString()
        If DataGridViewStudent.CurrentRow.Cells(2).Value.ToString().Equals("Female") Then
            rdbFemale.Checked = True
        Else
            rdbMale.Checked = True
        End If
        DateOfBirth.Text = DataGridViewStudent.CurrentRow.Cells(3).Value.ToString()
        txtBatch.Text = DataGridViewStudent.CurrentRow.Cells(5).Value.ToString()
        CmbShift.SelectedItem = DataGridViewStudent.CurrentRow.Cells(6).Value.ToString()
        txtAddress.Text = DataGridViewStudent.CurrentRow.Cells(7).Value.ToString()
        txtYear.Text = DataGridViewStudent.CurrentRow.Cells(8).Value.ToString()
        txtPhone.Text = DataGridViewStudent.CurrentRow.Cells(9).Value.ToString()
        txtNote.Text = DataGridViewStudent.CurrentRow.Cells(10).Value.ToString()


    End Sub

    Private Sub rdbMale_CheckedChanged(sender As Object, e As EventArgs) Handles rdbMale.CheckedChanged

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim sex As String = ""
        If rdbFemale.Checked Then
            sex = "Female"
        Else
            sex = "Male"
        End If

            Try
                SMSFucition.cnn()
            query = "UPDATE Students SET Name='" & txtName.Text & "',Sex='" & sex & "',DateOfBirth='" & DateOfBirth.Text & "',DepartmentID=" & cmbDepartment.SelectedValue & " ,Batch=" & txtBatch.Text & ",Shift='" & CmbShift.SelectedItem & "',[Year]=" & txtYear.Text & " ,[Address]='" & txtAddress.Text & "',Phone='" & txtPhone.Text & "',Notes='" & txtNote.Text & "'        WHERE ID=" & DataGridViewStudent.CurrentRow.Cells(0).Value & ""
            MessageBox.Show(query)
            cmd = New SqlCommand(query, cn)
                cmd.ExecuteNonQuery()
            Me.StudentDepartmentViewTableAdapter.Fill(Me.StudentListDataSet1.StudentDepartmentView)
            cmd.Dispose()
            cn.Close()
            Catch ex As Exception
                MessageBox.Show("Error")
            Finally
                cn.Close()
            End Try
    End Sub

    Private Sub StudentListForm_FormClosing_1(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Main.Show()
    End Sub
End Class