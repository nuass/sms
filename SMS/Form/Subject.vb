﻿Imports System.Data.SqlClient

Public Class Subject

    Private Sub Label12_Click(sender As Object, e As EventArgs) Handles Label12.Click, Label3.Click

    End Sub

    Private Sub Subject_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'SchoolDbDataSet2.Subjects' table. You can move, or remove it, as needed.
        Me.SubjectsTableAdapter.Fill(Me.SchoolDbDataSet2.Subjects)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If txtSubject.Text = "" Then
            txtSubject.Focus()
            lblRequired.Text = "This field Requred"
        Else
            lblRequired.Text = ""

            Try
                SMSFucition.cnn()
                query = "INSERT INTO Subjects (Name,HourPerWeek,Other) VALUES ('" & txtSubject.Text & "','" & txtHourPerWeek.Text & "','" & txtOther.Text & "')"
                cmd = New SqlCommand(query, cn)
                cmd.ExecuteNonQuery()
                Me.SubjectsTableAdapter.Fill(Me.SchoolDbDataSet2.Subjects)
            Catch ex As Exception
                MessageBox.Show("Error")
                cn.Close()
            Finally
                cn.Close()
                txtSubject.Text = ""
                txtHourPerWeek.Text = ""
                txtOther.Text = ""
            End Try
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            SMSFucition.cnn()
            query = "UPDATE Subjects SET Name='" & txtSubject.Text & "',HourPerWeek='" & txtHourPerWeek.Text & "',Other='" & txtOther.Text & "'  WHERE ID=" & DataGridView1.CurrentRow.Cells(0).Value & ""
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            Me.SubjectsTableAdapter.Fill(Me.SchoolDbDataSet2.Subjects)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            SMSFucition.cnn()
            query = "DELETE FROM Subjects WHERE ID=" & DataGridView1.CurrentRow.Cells(0).Value & ""
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            If Me.DataGridView1.SelectedRows.Count > 0 Then
                DataGridView1.Rows.RemoveAt(Me.DataGridView1.SelectedRows(0).Index)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        Finally
          txtSubject.Text = ""
            txtHourPerWeek.Text = ""
            txtOther.Text = ""
            cn.Close()
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        txtSubject.Text = DataGridView1.CurrentRow.Cells(1).Value.ToString()
        txtHourPerWeek.Text = DataGridView1.CurrentRow.Cells(2).Value.ToString()
        txtOther.Text = DataGridView1.CurrentRow.Cells(3).Value.ToString()
    End Sub

    Private Sub Subject_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Main.Show()
    End Sub
End Class