﻿Imports System.Data.SqlClient

Public Class TeachersForm

    Private Sub TextBox6_TextChanged(sender As Object, e As EventArgs) Handles txtAddress.TextChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sex As String
        If RadioButtonFemale.Checked Then
            sex = "Female"
        Else
            sex = "Male"
        End If


        query = "INSERT INTO Teachers VALUES ('" & txtTeacher.Text & "','" & sex & "','" & HireDateInput.Text & "'," & ComboBoxDepartment.SelectedValue & ",'" & txtNote.Text & "','" & OpenFileDialog1.FileName & "','" & txtAddress.Text & "','" & txtPhone.Text & "'," & txtSalary.Text & ")"
        Try
            SMSFucition.cnn()
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            Me.TeacherDepartmentViewTableAdapter.Fill(Me.SchoolDbDataSet7.TeacherDepartmentView)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub Teacher_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TeacherDepartmentViewTableAdapter.Fill(Me.SchoolDbDataSet7.TeacherDepartmentView) 'DataGridView
        Me.DepartmentsTableAdapter1.Fill(Me.SchoolDbDataSet6.Departments)  'Department combobox


    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        txtAddress.Text = DataGridView1.CurrentRow.Cells(6).Value.ToString
    End Sub

    Private Sub RadioButtonFemale_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButtonFemale.CheckedChanged

    End Sub

    Private Sub RadioButtonMale_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButtonMale.CheckedChanged

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        With OpenFileDialog1
            .Title = "Select student image"
            .Multiselect = False
            .Filter = "Insert Image file as |*.jpg|PNG|*.png|GIF|*.gif "

            .FileName = ""
            .ShowDialog()
            If .FileName = "" Then
                Exit Sub
            Else
                PictureBox1.Load(.FileName)
            End If
        End With
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        PictureBox1.Image = Nothing
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        With OpenFileDialog1
            .Title = "Select student image"
            .Multiselect = False
            .Filter = "Insert Image file as |*.jpg|PNG|*.png|GIF|*.gif "

            .FileName = ""
            .ShowDialog()
            If .FileName = "" Then
                Exit Sub
            Else
                PictureBox1.Load(.FileName)
            End If
        End With
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            SMSFucition.cnn()
            query = "SELECT * FROM Teachers WHERE ID=" & txtSearch.Text & ""
            Dim Ad As New SqlDataAdapter(query, cn)
            Dim dataset As New DataSet()
            Ad.Fill(dataset, "Emp")
            DataGridView1.DataSource = dataset.Tables(0)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Search Error")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim sex As String = ""
        If RadioButtonFemale.Checked Then
            sex = "Female"
        Else
            sex = "Male"
        End If
        Try
            SMSFucition.cnn()
            query = "UPDATE Teachers SET Name='" & txtTeacher.Text & "',Sex='" & sex & "',HireDate='" & HireDateInput.Text & "',DepartmentId=" & ComboBoxDepartment.SelectedValue & ",Notes='" & txtNote.Text & "',Address='" & txtAddress.Text & "',Phone='" & txtPhone.Text & "',Salary=" & txtSalary.Text & "       WHERE ID=" & DataGridView1.CurrentRow.Cells(0).Value & ""
            cmd = New SqlCommand(query, cn)
            cmd.ExecuteNonQuery()
            Me.TeacherDepartmentViewTableAdapter.Fill(Me.SchoolDbDataSet7.TeacherDepartmentView)
            cn.Close()
        Catch ex As Exception
            MessageBox.Show("Error")
        Finally
            cn.Close()
        End Try
    End Sub
End Class