﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("School Management System")> 
<Assembly: AssemblyDescription("SMS is a VB.NET Assignment Project for Year III, Semester II, Group Computer E1 Created by : 1. Neang Darea 2.Ly Navy 3.Ny Dina  4. Ok Raksmey")> 
<Assembly: AssemblyCompany("Norton")> 
<Assembly: AssemblyProduct("SMS")> 
<Assembly: AssemblyCopyright("Copyright ©  2015 By Neang Dara")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c11bc554-a76d-4617-88ae-b4fe24d8b1b8")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
